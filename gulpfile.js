let gulp = require('gulp');
    stylus = require('gulp-stylus');
    sass = require('gulp-sass');

gulp.task('stylus', function() {
    gulp.src('public/onsen/stylus/custom.styl')
        .pipe(stylus('./'))
        .pipe(gulp.dest('scss/'));
});

gulp.task('scss', function() {
    gulp.src('scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('public/'));
});

gulp.task('watch',['stylus'], function(){
    gulp.watch([
        [
            'scss/style.scss',
        ],
    ], ['scss','generate-service-worker']);
});

gulp.task('generate-service-worker', function(callback) {
    const path = require('path');
    const swPrecache = require('sw-precache');
    const rootDir = 'public/';

    swPrecache.write(`${rootDir}/service-worker.js`, {
        staticFileGlobs: [rootDir + '/**/*.{js,html,css,png,jpg,gif,svg,eot,ttf,woff}'],
        stripPrefix: rootDir,
    }, callback);
});