/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["image/20161222/DSC06166-2.jpg","2f0b4a9406448ca6cdc869e0314bf541"],["image/20161222/DSC06167-2.jpg","3602312b0fb66ee90db6b2e81fee01e6"],["image/20161222/DSC06168-2.jpg","8053bb87107ea19786f8bbecba273a1f"],["image/20161222/DSC06169-2.jpg","8047bcf63f628df324eb00b731254ce1"],["image/20161222/DSC06170-2.jpg","70a0bfef170d43c9a878a23e8ef31879"],["image/20161222/DSC06174.jpg","51367fff5db936c22ee0b5cac51718fa"],["image/20161222/DSC06176.jpg","6cdb5b93c38488861908bf539fe78b5d"],["image/20161222/DSC06178.jpg","c9d79a1fafde6a4f79be89d5b5170a3c"],["image/20161222/DSC06179.jpg","34888b1cea408e86040df701a05f8d73"],["image/20161222/DSC06185.jpg","8fced242d9fb38a65c08ed5ffe30a43d"],["image/20161222/DSC06187.jpg","f45b4bed19e4e691e24e75183199c4a7"],["image/20161222/DSC06189.jpg","ef1cd0e711fa995a29c1f88a14fa1ae9"],["image/20161222/DSC06190.jpg","79dffe2534da42de66e4bbc17d9a34f4"],["image/20161222/DSC06193.jpg","f23b0e500381a64e35d1b03107c21631"],["image/20161222/DSC06196.jpg","928df890d0d57d94a8ac23c1ffcfa046"],["image/20161222/DSC06198.jpg","2a33e4f715245a699f9e27d2d7b5a2ee"],["image/20161222/DSC06200.jpg","79cd39df92cb6913dd29ae3c04388c1a"],["image/20161222/DSC06204.jpg","4f47610c2a5b6c8767dd71e4c226d5d6"],["image/20161222/DSC06206-2.jpg","da77f491a6b42f069241ed26c5fda69c"],["image/20161222/DSC06206.jpg","969ebc702bfb724f556c07210761a9d6"],["image/20161222/DSC06208.jpg","9df42bab3cc66adc8debfd2289870d92"],["image/20161222/DSC06213.jpg","2e245cef6375ddc5225cfba8b749d09a"],["image/20161222/circle-DSC06167-2.jpg","a66cc8372b6f01e100ba0f9f4bbf97f6"],["image/20161222/circle-DSC06187.jpg","24614ed56efd6e9ee33e61d147c1848d"],["image/20161222/circle-DSC06206.jpg","5a2f5aff37ce5577365c6a962b016bab"],["image/20161222/hero-DSC06170-3.jpg","d1d187ceae4d5538f5bd927e66da5d82"],["image/food_001.jpg","8ca2b5ef45e3a967429dc0c03e3a0d94"],["image/food_002.jpg","7d7d5f2d04e5c6a085032c7f3e93ec58"],["image/food_003.jpg","ae041f9bdf3c70ed4fe71586bfccfe63"],["image/food_004.jpg","2c93d9ca474447ff8ccbd01ace26f026"],["image/food_005.jpg","93f3557919c9be3d9f88059bebbb60ba"],["image/food_006.jpg","1a573cb268606e797e4378aa427590e0"],["image/hero.jpg","b2eb245c290ee907bed6ae8694571803"],["image/logo.svg","0506da9c139407d9d834630babaab624"],["image/ogp.png","f2ca8288572ae2a6ed4a6c38652d6f04"],["image/owner.jpg","aa24a8adcad2d836a520d04cd593a881"],["image/shop_001.jpg","8f0b60e350b92897b21d2f63984e7105"],["index.html","c58490944906bd71ceea89ee11b9b1a2"],["jquery-3.1.1.slim.min.js","550ddfe84a114f79a767c087df97f3bc"],["onsen/css/font_awesome/css/font-awesome.css","b91450cb4a40f706bc9230215b02ec24"],["onsen/css/font_awesome/css/font-awesome.min.css","7f297f36af932b6fa546415b0712f476"],["onsen/css/font_awesome/fonts/fontawesome-webfont.eot","25a32416abee198dd821b0b17a198a8f"],["onsen/css/font_awesome/fonts/fontawesome-webfont.svg","d7c639084f684d66a1bc66855d193ed8"],["onsen/css/font_awesome/fonts/fontawesome-webfont.ttf","1dc35d25e61d819a9c357074014867ab"],["onsen/css/font_awesome/fonts/fontawesome-webfont.woff","c8ddf1e5e5bf3682bc7bebf30f394148"],["onsen/css/ionicons/css/ionicons.css","8b92f86371ac7fe42203dee6230079a8"],["onsen/css/ionicons/css/ionicons.min.css","300b6b7cf7aee2498d5740f6a23bf439"],["onsen/css/ionicons/fonts/ionicons.eot","19e65b89cee273a249fba4c09b951b74"],["onsen/css/ionicons/fonts/ionicons.svg","aff28a207631f39ee0272d5cdde43ee7"],["onsen/css/ionicons/fonts/ionicons.ttf","dd4781d1acc57ba4c4808d1b44301201"],["onsen/css/ionicons/fonts/ionicons.woff","2c159d0d05473040b53ec79df8797d32"],["onsen/css/material-design-iconic-font/css/material-design-iconic-font.css","f017fff7457dd253aa09707b68b27f86"],["onsen/css/material-design-iconic-font/css/material-design-iconic-font.min.css","cc3d4e1938c1693be4a6d53e5d4363fc"],["onsen/css/material-design-iconic-font/fonts/Material-Design-Iconic-Font.eot","e833b2e2471274c238c0553f11031e6a"],["onsen/css/material-design-iconic-font/fonts/Material-Design-Iconic-Font.svg","381f7754080ed2299a7c66a2504dff02"],["onsen/css/material-design-iconic-font/fonts/Material-Design-Iconic-Font.ttf","b351bd62abcd96e924d9f44a3da169a7"],["onsen/css/material-design-iconic-font/fonts/Material-Design-Iconic-Font.woff","d2a55d331bdd1a7ea97a8a1fbb3c569c"],["onsen/css/onsen-css-components-blue-basic-theme.css","92cba17feac9faf437a6ee498f7fa44c"],["onsen/css/onsen-css-components-blue-theme.css","cbcca86a225d7497e8e7e578eee3eeb3"],["onsen/css/onsen-css-components-dark-theme.css","ed7f3fa2e85fe4530e38fb4817a98f8e"],["onsen/css/onsen-css-components-default.css","92cba17feac9faf437a6ee498f7fa44c"],["onsen/css/onsen-css-components-purple-theme.css","9afb92ba7d0e598a2d8e3224c4c9dd7b"],["onsen/css/onsen-css-components-sunshine-theme.css","390ccee535848023a63aae934fc5c64f"],["onsen/css/onsen-css-components.css","92cba17feac9faf437a6ee498f7fa44c"],["onsen/css/onsenui.css","4193160229e4e7b916e543bdad9cc81d"],["onsen/js/angular-onsenui.js","0d537b94fbd287c27242f3bc0c208a00"],["onsen/js/angular-onsenui.min.js","f3340c625a97e90e6cf919d838824848"],["onsen/js/onsenui.min.js","a890f3219e812f4852867838ae78a6a1"],["service-worker-registration.js","a3856418d25e92e75896f50e1253265a"],["style.css","08a422059ace2401b33bd6b95fe4ab13"]];
var cacheName = 'sw-precache-v2--' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var createCacheKey = function (originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.toString().match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function (whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              return cache.add(new Request(cacheKey, {
                credentials: 'same-origin',
                redirect: 'follow'
              }));
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameter and see if we have that URL
    // in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = 'index.html';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = '';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted([], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







